package ua.`in`.mitya.kotlin

import ua.`in`.mitya.kotlin.authorization.Authorization
import ua.`in`.mitya.kotlin.product.ProductAttributes


fun main(args: Array<String>) {

    val listSku = arrayListOf<String>()
    /**
     * Get product content with some sku.
     */
    ProductAttributes(Authorization().getToken()!!).getProductJSON("some_sku")
    /**
     * Get stock price.
     */
    ProductAttributes(Authorization().getToken()!!).getStock()
    /**
     * Get product content with list sku;
     */
    ProductAttributes(Authorization().getToken()!!).getProductsJSON(listSku)

}

class App {
    val MAGENTO_API_KEY = "api_key"
    val MAGENTO_API_SECRET = "api_secret_key"
    val BASE_URL = "http://example.com"
    val API_URL = "http://example.com/api/rest"

    /**
     * Warning! Don`t change value of API_LIMIT !!
     */
    val API_LIMIT = 100

    val FILE_PATH_TOKEN = ".\\res\\token.xml"
    val FILE_PATH_SERVICE = ".\\res\\service.xml"
    val FILE_PHOTO_PATH = ".\\res\\images\\"
}
