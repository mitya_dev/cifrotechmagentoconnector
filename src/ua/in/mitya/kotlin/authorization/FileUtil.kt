package ua.`in`.mitya.kotlin.authorization

import java.io.File
import java.io.IOException
import java.nio.charset.Charset
import java.nio.file.Files


class FileUtil {
    private val CHARSET = Charset.forName("UTF-8")

    @Throws(IOException::class)
    fun readFile(file: File): String {
        val stringBuffer = StringBuilder()
        val reader = Files.newBufferedReader(file.toPath(), CHARSET)
        var line: String? = null
        while ({ line = reader.readLine(); line }() != null) {
            stringBuffer.append(line)
        }

        reader.close()
        return stringBuffer.toString()
    }

    @Throws(IOException::class)
    fun saveFile(content: String, file: File) {
        val writer = Files.newBufferedWriter(file.toPath(), CHARSET)
        writer.write(content, 0, content.length)
        writer.close()
    }
}