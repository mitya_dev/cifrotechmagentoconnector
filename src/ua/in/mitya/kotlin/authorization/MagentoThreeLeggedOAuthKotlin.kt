package ua.`in`.mitya.kotlin.authorization

import org.scribe.builder.api.DefaultApi10a
import org.scribe.model.Token
import ua.`in`.mitya.kotlin.App

class MagentoThreeLeggedOAuthKotlin : DefaultApi10a() {

    override fun getRequestTokenEndpoint(): String {
        return "${App().BASE_URL}/oauth/initiate"
    }

    override fun getAuthorizationUrl(arg0: Token): String {
        return """${App().BASE_URL}/oauth/authorize?oauth_token=${arg0.token}"""
    }

    override fun getAccessTokenEndpoint(): String {
        return "${App().BASE_URL}/oauth/token"
    }
}