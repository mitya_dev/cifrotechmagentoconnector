package ua.`in`.mitya.kotlin.authorization

import org.scribe.builder.ServiceBuilder
import org.scribe.model.Token
import org.scribe.model.Verifier
import org.scribe.oauth.OAuthService
import ua.`in`.mitya.kotlin.App

@Deprecated("use class Autorization")
class MagentoConnectorKotlin {

    var service: OAuthService
    private var accessToken: Token
    private var authUrl: String = ""

    fun getToken(): Token? {
        println("""Token: ${instance.getAccessToken()}""")
        return instance.getAccessToken()
    }

    fun getAccessToken(): Token? {
        if (accessToken == null)
            return null
        else return accessToken
    }

    init {
        service = ServiceBuilder()
                .provider(MagentoThreeLeggedOAuthKotlin())
                .apiKey(App().MAGENTO_API_KEY)
                .apiSecret(App().MAGENTO_API_SECRET)
                .debug()
                .build()
        println("Magento'srkflow")
        println()
        println("FetchingRequest Token...")
        val requestToken: Token = service.requestToken
        println("GetRequest Token!")
        println()
        println("FetchingAuthorization URL...")
        val autorizaitionUrl: String = service.getAuthorizationUrl(requestToken)
        println("GotAuthorization URL!")



        println("Nownd authorize Main here:")
        println(autorizaitionUrl)
        println("Add the authorization code here")
        print(">>")
        val stringInput = readLine()!!
        val verifier = Verifier(stringInput)
        println()
        println("TradingRequest Token for an Access Token...")
        accessToken = service.getAccessToken(requestToken, verifier)
        println("GetAccess Token!")
        println("(if curious it looks like this: $accessToken )")
        println()
    }

    private object Holder {
        val INSTANCE = MagentoConnectorKotlin()
    }

    companion object {
        val instance: MagentoConnectorKotlin by lazy { Holder.INSTANCE }
    }
}