package ua.`in`.mitya.kotlin.authorization

import com.thoughtworks.xstream.XStream
import org.scribe.builder.ServiceBuilder
import org.scribe.model.Token
import org.scribe.model.Verifier
import org.scribe.oauth.OAuthService
import ua.`in`.mitya.kotlin.App
import java.io.File
import java.util.*


class Authorization {

    private var service: OAuthService? = null
    private var requestToken: Token? = null
    private var accessToken: Token? = null
    private var authUrl: String? = null

    init {
        start()
    }

    fun getToken(): Token? {
        return instance.getAccessToken()
    }

    private fun getAccessToken(): Token? {
        if (accessToken == null) return null
        else return accessToken as Token
    }

    private fun start(): Token? {

        val f = File(App().FILE_PATH_TOKEN)
        if (f.exists()) {
            println("Feching Existing Token")
            loadToken()
            return this.accessToken
        }

        val `in` = Scanner(System.`in`)

        service = ServiceBuilder()
                .provider(MagentoThreeLeggedOAuthKotlin())
                .apiKey(App().MAGENTO_API_KEY)
                .apiSecret(App().MAGENTO_API_SECRET)
                .debug()
                .build()
        println("Magento'srkflow")
        println()

        println("--- Magento OAuth Authorization ---\n")
        println("Fetching The Request Token...")
        requestToken = service?.requestToken
        println("Got The Request Token!\n")
        println(" Go & Authorize Magento Here:")
        authUrl = service?.getAuthorizationUrl(requestToken)
        println(authUrl)
        println("\nPaste The Verifier Here:")
        print(">> ")
        val verifier = Verifier(`in`.nextLine())
        println("\nTrading The Request Token For An Access Token...")
        accessToken = service?.getAccessToken(requestToken, verifier)
        println("Got the Access Token!")
        saveToken()
        return accessToken
    }

    @SuppressWarnings("unchecked")
    private fun createService(): OAuthService? {

        val f = File(App().FILE_PATH_SERVICE)
        if (f.exists()) {
            println("Feching Existing Service")
            loadService()
            return this.service
        }
        service = ServiceBuilder()
                .provider(MagentoThreeLeggedOAuthKotlin())
                .apiKey("Consumer Key")
                .apiSecret("Consumer Secret")
                .build()
        saveService()
        return service
    }

    private fun saveService() {
        val file = File(App().FILE_PATH_SERVICE)
        val xstream = XStream()
        xstream.alias("service", OAuthService::class.java)
        val xml = xstream.toXML(service)
        try {
            FileUtil().saveFile(xml, file)
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    private fun saveToken() {
        val file = File(App().FILE_PATH_TOKEN)
        val xstream = XStream()
        xstream.alias("token", Token::class.java)
        val xml = xstream.toXML(accessToken)

        try {
            FileUtil().saveFile(xml, file)
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    private fun loadService() {
        val file = File(App().FILE_PATH_SERVICE)
        val xstream = XStream()
        xstream.alias("service", OAuthService::class.java)
        try {
            val xml = FileUtil().readFile(file)
            service = xstream.fromXML(xml) as OAuthService
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    private fun loadToken() {
        val file = File(App().FILE_PATH_TOKEN)
        val xstream = XStream()
        xstream.alias("token", Token::class.java)
        try {
            val xml = FileUtil().readFile(file)
            accessToken = xstream.fromXML(xml) as Token
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    private object Holder {
        val INSTANCE = Authorization()
    }

    private companion object {
        val instance: Authorization by lazy { Holder.INSTANCE }
    }
}