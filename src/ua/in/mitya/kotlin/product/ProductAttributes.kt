package ua.`in`.mitya.kotlin.product

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.scribe.builder.ServiceBuilder
import org.scribe.model.OAuthRequest
import org.scribe.model.Token
import org.scribe.model.Verb
import org.scribe.oauth.OAuthService
import ua.`in`.mitya.kotlin.App
import ua.`in`.mitya.kotlin.authorization.MagentoThreeLeggedOAuthKotlin
import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.net.URL

class ProductAttributes(private val accessToken: Token) {

    fun getProductJSON(sku: String) {
        var request = OAuthRequest(Verb.GET, "${App().API_URL}/products?sku=$sku")
        getService().signRequest(accessToken, request)
        var response = request.send()

        println(response.code)
        println(response.body)
        getProductId(response.body)
    }

    fun getStock() {
        var jsonList: MutableList<String> = mutableListOf()
        for (i in 1..10000) {
            var request = OAuthRequest(Verb.GET, "${App().API_URL}/getstock?limit=${App().API_LIMIT}&page=$i")
            getService().signRequest(accessToken, request)
            var response = request.send()

            println(response.code)
            println(response.body)

            if (i != 1 && response.body.equals(jsonList[i - 2]))
                break
            else
                jsonList.add(response.body)

        }
        for (str: String in jsonList) {
            println(str)
        }
    }

    fun getProductsJSON(skuList: ArrayList<String>) {
        for (item in skuList) {
            getProductJSON(item)
        }
    }

    private fun getProductId(json: String) {
        val jsonObject = JsonParser().parse(json) as JsonObject
        println("Product ID: " + jsonObject.get("url").toString().split("/")[8])
        getProductImagesUrl(jsonObject.get("url").toString().split("/")[8])
    }

    fun getProductImagesUrl(productId: String): ArrayList<String>? {
        var request = OAuthRequest(Verb.GET, "${App().API_URL}/products/$productId/images")
        getService().signRequest(accessToken, request)
        var response = request.send()
        println("Image: " + response.code)
        println("Image: " + response.body)
        var jsonArray = JsonParser().parse(response.body) as JsonArray
        for (item in jsonArray.asJsonArray) {
            var temp = JsonParser().parse(item.toString()) as JsonObject
            println(temp.get("url"))
            downloadProductImages(temp.get("url").toString())
        }
        return null
    }

    fun downloadProductImages(urlImage: String) {
        var indexName = urlImage.lastIndexOf("/")

        if (indexName == urlImage.length) {
            urlImage.substring(1, indexName)
        }

        indexName = urlImage.lastIndexOf("/")
        val name = urlImage.substring(indexName, urlImage.length - 5)
        val inputStream = URL(urlImage.replace("\"", "")).openStream()
        val outputStream = BufferedOutputStream(FileOutputStream(App().FILE_PHOTO_PATH + name))

        val b: Int
        b = inputStream.read()
        while (b != -1) {
            outputStream.write(b)
        }

        outputStream.close()
        inputStream.close()
    }

    fun getService(): OAuthService {
        return ServiceBuilder()
                .provider(MagentoThreeLeggedOAuthKotlin::class.java)
                .apiKey(App().MAGENTO_API_KEY)
                .apiSecret(App().MAGENTO_API_SECRET)
                .debug()
                .build()
    }
}